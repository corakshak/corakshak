import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-our-partner',
  templateUrl: './our-partner.component.html',
  styleUrls: ['./our-partner.component.css']
})
export class OurPartnerComponent implements OnInit {

  partner1="assets/digilocker.png"
  partner2="assets/arogyasetu.png"
  partner3="assets/umang.png"
  partner4="assets/mygov.png"

  constructor() { }

  ngOnInit(): void {
  }

}
