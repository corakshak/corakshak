import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaccinesummeryComponent } from './vaccinesummery.component';

describe('VaccinesummeryComponent', () => {
  let component: VaccinesummeryComponent;
  let fixture: ComponentFixture<VaccinesummeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaccinesummeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinesummeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
