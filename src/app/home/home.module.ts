import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import {  RouterModule, Routes } from '@angular/router';
import { VaccinationCountComponent } from './vaccination-count/vaccination-count.component';
import { VaccinationCenterComponent } from './vaccination-center/vaccination-center.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { RaiseIssueComponent } from './raise-issue/raise-issue.component';
import { StepToVaccinateComponent } from './step-to-vaccinate/step-to-vaccinate.component';
import {StepsModule} from 'primeng/steps';
import { FAQComponent } from './faq/faq.component';
import { VaccinesummeryComponent } from './vaccinesummery/vaccinesummery.component';
import {  CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { OurPartnerComponent } from './our-partner/our-partner.component';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { HttpClientModule } from '@angular/common/http';
import { DropdownModule } from 'primeng/dropdown';




const routes:Routes=[
  { path:'',component:LandingComponent},
  { path:'VaccinationCount',component:VaccinationCountComponent},
  { path:'VaccinationCenter',component:VaccinationCenterComponent},
  { path:'whatsNew',component:WhatsnewComponent},
  { path:'raiseIssue',component:RaiseIssueComponent},
  { path:'StepToVaccinate',component:StepToVaccinateComponent},
  { path:'FAQ',component:FAQComponent},
  { path:'vaccineSummery',component:VaccinesummeryComponent}
]

@NgModule({
  declarations: [
    LandingComponent,
    VaccinationCountComponent,
    VaccinationCenterComponent,
    WhatsnewComponent,
    RaiseIssueComponent,
    StepToVaccinateComponent,
    FAQComponent,
    VaccinesummeryComponent,
    OurPartnerComponent,

    
  ],
  imports: [
    CommonModule,
    StepsModule,
    RouterModule.forChild(routes),
    TabViewModule,  
    ButtonModule,
    ScrollPanelModule,
    HttpClientModule, 
    DropdownModule,
    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
 
})
export class HomeModule { }
