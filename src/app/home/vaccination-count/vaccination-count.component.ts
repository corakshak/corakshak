import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vaccination-count',
  templateUrl: './vaccination-count.component.html',
  styleUrls: ['./vaccination-count.component.css']
})
export class VaccinationCountComponent implements OnInit {

  img1="assets/vaccine1.png"
  img2="assets/vaccine2.png"

  constructor() { }

  ngOnInit(): void {
  }

}
