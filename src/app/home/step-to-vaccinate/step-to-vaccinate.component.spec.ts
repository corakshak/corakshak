import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepToVaccinateComponent } from './step-to-vaccinate.component';

describe('StepToVaccinateComponent', () => {
  let component: StepToVaccinateComponent;
  let fixture: ComponentFixture<StepToVaccinateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepToVaccinateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepToVaccinateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
