import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';
import {  CUSTOM_ELEMENTS_SCHEMA , NO_ERRORS_SCHEMA} from '@angular/core';
schemas: [ CUSTOM_ELEMENTS_SCHEMA ];
schemas: [ NO_ERRORS_SCHEMA ];

@Component({
  selector: 'app-step-to-vaccinate',
  templateUrl: './step-to-vaccinate.component.html',
  styleUrls: ['./step-to-vaccinate.component.css']
})
export class StepToVaccinateComponent implements OnInit {

  vaccinatedImg1="assets/vaccinated_img1.png"
  vaccinatedImg2="assets/vaccinated_img2.png"
  vaccinatedImg3="assets/vaccinated_img3.png"

  items: MenuItem[];

  constructor() {
    this.items = [
      {label: 'Step 1'},
      {label: 'Step 2'},
      {label: 'Step 3'}
  ];
   }
  
  ngOnInit(): void {
 
  }

}
