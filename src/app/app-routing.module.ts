import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:'',loadChildren:()=>import('./home/home.module').then(mod=>mod.HomeModule)},
  { path:'comman',loadChildren:()=>import('./comman/comman.module').then(mod=>mod.CommanModule)},
  { path:'dashSummary',loadChildren:()=>import('./dashboard/dashboard.module').then(mod=>mod.DashboardModule)},
  { path:'Admin',loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
