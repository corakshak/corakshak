import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class APIserviceService {

  constructor(private http:HttpClient) { }
   
  GenerateOtp(mobNum:any){
    let UserOtp={
      "mobile":mobNum
    }
    return this.http.post('https://cdn-api.co-vin.in/api/v2/auth/public/generateOTP',UserOtp)
  }

  ConfirmOtp(UserOtp:any){
    return this.http.post('https://cdn-api.co-vin.in/api/v2/auth/public/confirmOTP',UserOtp)
  }

  GetState(){
    return this.http.get('https://cdn-api.co-vin.in/api/v2/admin/location/states')
  }

  getDistrict(state_id:any){
    let url='https://cdn-api.co-vin.in/api/v2/admin/location/districts/'+state_id;
     return this.http.get(url);
  }

  // getSearchData(district_id:any){
  // let url='https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/'+district_id;
  //   return this.http.get(url,district_id)
  // }

 
}
