import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegMemberComponent } from './reg-member.component';

describe('RegMemberComponent', () => {
  let component: RegMemberComponent;
  let fixture: ComponentFixture<RegMemberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegMemberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegMemberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
