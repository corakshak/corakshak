import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegMemberComponent } from './reg-member/reg-member.component';



@NgModule({
  declarations: [
    RegMemberComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ResistrationModule { }
