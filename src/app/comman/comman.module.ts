import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutComponent } from './about/about.component';
import { LogRegComponent } from './log-reg/log-reg.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

const routes: Routes=[
  
  { path:'about',component:AboutComponent},
  { path:'logReg',component:LogRegComponent},

]

@NgModule({
  declarations: [    
    AboutComponent,
    LogRegComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    FormsModule


  ]
})
export class CommanModule { }
