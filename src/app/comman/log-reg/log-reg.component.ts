import { Component, OnInit } from '@angular/core';
import { APIserviceService } from 'src/app/apiservice.service';

@Component({
  selector: 'app-log-reg',
  templateUrl: './log-reg.component.html',
  styleUrls: ['./log-reg.component.css']
})
export class LogRegComponent implements OnInit {

  public GenOTP:any=true;
  public cnfOTP:any=false;

  public mobNum:any;
  public dataContainer:any

  public otp:any;
  public txnId:any;

  

  constructor(private apiSer:APIserviceService) { }

  ngOnInit(): void {
  }

  GenerateOtp(){
    this.apiSer.GenerateOtp(this.mobNum).subscribe(res=>{
      this.dataContainer=res
      console.log(this.dataContainer)
      this.txnId=this.dataContainer.txnId
    })
    // This is for hide and show button
    this.GenOTP=false;
    this.cnfOTP=true;
  }
  // -------------------------------
  ConfirmOtp(){
      let UserOtp={
        "otp":this.otp,
        "txnId":this.txnId
      }
      this.apiSer.ConfirmOtp(UserOtp).subscribe(res=>{
        console.log(res)
      })
  }



}
 