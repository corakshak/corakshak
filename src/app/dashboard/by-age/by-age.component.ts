import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { ChartItem } from 'chart.js';

@Component({
  selector: 'app-by-age',
  templateUrl: './by-age.component.html',
  styleUrls: ['./by-age.component.css']
})
export class ByAgeComponent implements OnInit {

  ByAge:any=[];

  AgePia:any=[];

  constructor() { }

  ngOnInit(): void {
    //  graph floated by using age data
    var ctx = document.getElementById('GraphForAge');
    const ForAge = 'GraphForAge'; 
    this.ByAge = new Chart(ForAge, {
      type: 'line',
      data: {
        datasets: [{
          label: 'Total',
          data: [0.0005, 0.0004, 0.0003, 0.0008, 0.0004, 0.0003],
          backgroundColor:
            'rgb(165, 42, 42)',
          borderColor:
            'rgb(165, 42, 42)',
          borderWidth: 2
        }, {
          label: '18-44',
          data: [0.0001, 0.0007, 0.0002, 0.0007, 0.0001, 0.0006],
          backgroundColor:
            '(0,255,255)',
          borderColor:
            '(0,255,255)',
          borderWidth: 2
        }, {
          label: '45-60',
          data: [0.0002, 0.0004, 0.0005, 0.0006, 0.0003, 0.0007],
          backgroundColor:
            'rgb(0,0,128)',
          borderColor:
            'rgb(0,0,128)',
          borderWidth: 2
        },{
          label: 'Above 60',
          data: [0.0003, 0.0005, 0.0008, 0.0009, 0.0004, 0.0004],
          backgroundColor:
            'rgb(100, 206, 177)',
          borderColor:
            'rgb(100, 206, 177)',
          borderWidth: 2
        }],
        labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
    
    // for Pia chart data
    const PiaForAge = 'PiaForAge'; 
    this.AgePia = new Chart(PiaForAge, {
      type: 'pie',
      data: {
        datasets: [{          
          data: [100022, 124100, 124001],
          backgroundColor:
          ['rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)'],
          borderColor:
          ['rgb(255, 99, 132)',
          'rgb(54, 162, 235)',
          'rgb(255, 205, 86)'],
          borderWidth: 2,
          hoverOffset: 5
        },      
      ],
      labels: ['18-44','45-60','Above 60'],
      
      },     
    });    
  }


}
