import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashSumaryComponent } from './dash-sumary.component';

describe('DashSumaryComponent', () => {
  let component: DashSumaryComponent;
  let fixture: ComponentFixture<DashSumaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashSumaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashSumaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
