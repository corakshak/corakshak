import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-by-dose',
  templateUrl: './by-dose.component.html',
  styleUrls: ['./by-dose.component.css']
})
export class ByDoseComponent implements OnInit {

  ByDose:any=[];

  constructor() { }

  ngOnInit(): void {
  const ForDose = 'GraphForDose';
 
  this.ByDose = new Chart(ForDose, {
    type: 'line',
    data: {
      datasets: [{
        label: 'Total Doses',
        data: [0.0005, 0.0004, 0.0003, 0.0008, 0.0004, 0.0003],
        backgroundColor:
          'rgb(0, 255, 127)',
        borderColor:
          'rgb(0, 255, 127)',
        borderWidth: 2
      }, {
        label: 'Dose one',
        data: [0.0001, 0.0007, 0.0002, 0.0007, 0.0001, 0.0006],
        backgroundColor:
          'rgba(255, 99, 132, 1)',
        borderColor:
          'rgba(255, 99, 132, 1)',
        borderWidth: 2
      }, {
        label: 'Dose Two',
        data: [0.0002, 0.0004, 0.0005, 0.0006, 0.0003, 0.0007],
        backgroundColor:
          'rgb(255, 165, 0)',
        borderColor:
          'rgb(255, 165, 0)',
        borderWidth: 2
      }],

      labels: ['Jan', 'Feb', 'Mar', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
}
}