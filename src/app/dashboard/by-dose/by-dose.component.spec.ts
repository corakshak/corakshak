import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ByDoseComponent } from './by-dose.component';

describe('ByDoseComponent', () => {
  let component: ByDoseComponent;
  let fixture: ComponentFixture<ByDoseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ByDoseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ByDoseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
