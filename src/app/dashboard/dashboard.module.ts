import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashSumaryComponent } from './dash-sumary/dash-sumary.component';
import { RouterModule, Routes } from '@angular/router';
import { VaccinationTrendComponent } from './vaccination-trend/vaccination-trend.component';
import { NgChartsModule } from 'ng2-charts';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import { ChartModule } from 'primeng/chart';
import { ByDoseComponent } from './by-dose/by-dose.component';
import { ByAgeComponent } from './by-age/by-age.component';


const routes:Routes=[
  { path:'dashSum',component:DashSumaryComponent},
  { path:'Trend',component:VaccinationTrendComponent},
  { path:'Dose',component:ByDoseComponent},
  { path:'Age',component:ByAgeComponent}
 ]

@NgModule({
  declarations: [
    DashSumaryComponent,
    VaccinationTrendComponent,
    ByDoseComponent,
    ByAgeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgChartsModule,
    TabViewModule,
    ButtonModule,
    ChartModule
    
   
  ]
})
export class DashboardModule { }
